from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Randy Hidayah Putra Desnantara' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,8,31) #TODO Implement this, format (Year, Month, Date)
npm = 1706044130 # TODO Implement this
university = "University of Indonesia"
hobby = "Hiking and Gaming"
description = "I used to play dota 2 quite a lot. Altough rn i prefer playing Dauntless(not promoted btw)"

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm,
    'university': university, 'hobby': hobby, 'description': description}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
